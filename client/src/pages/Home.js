import React, { Component } from 'react';
import axios from 'axios';
class Home extends Component {
    state = {
      url:'',
      maxDepth: 5,
      maxPages: 100,
      links: null,
      linksString: ""

    }
    


    componentDidMount(){
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let _self = this;
        axios.post('http://localhost:9000/api', {
            
                url:this.state.url,
                maxDepth: this.state.maxDepth,
                maxPages: this.state.maxPages
            
        })
        .then(function (response) {
            _self.setState({
                links:response.data.links,
                linksString: JSON.stringify(response.data.links)
            });
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
    }
    handleUrlChange = (e) =>{
        this.setState({
            url:e.target.value
        });
    }
    
    handleDepthChange = (e) =>{
        this.setState({
            maxDepth:e.target.value
        });
    }
    
    handlePagesChange = (e) =>{
        this.setState({
            maxPages:e.target.value
        });
    }

    render() {
      
      return (
        <div className="Home" id="main">
            <form onSubmit={this.handleSubmit}>
                
                URL:<br/>
                <input type="text" name="url" onChange={this.handleUrlChange}/><br/>
                Max Depth:<br/>
                <input type="text" name="maxdepth" onChange={this.handleDepthChange}/><br/>
                Max Pages:<br/>
                <input type="text" name="maxpages" onChange={this.handlePagesChange}/><br/>
                <button onClick={this.handleSubmit} type="button">send</button>
            

            </form>
            
            URL: {this.state.url}
            <br/>
            <br/>
            links: {this.state.linksString}
        </div>
      );
    }
    
  }

export default Home;