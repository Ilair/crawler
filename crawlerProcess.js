
var rp = require("request-promise");
var cheerio = require("cheerio");
var fs = require("fs");

fs.writeFile('content.json', '', function (err) {

});

var primaryUrl = "https://www.google.com/";
var maxPages = 70;
var maxDepth = 2;
var primaryLinks=[];
var c = 0;
const urlregex = new RegExp(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/g);

getLinks();


async function scanUrl(url,links,currentDepth)
{
    if(currentDepth<=maxDepth){
        let body=null;
        try{ body = await rp(url)}
        catch(err){
            console.log("err");
            return;
        }
        //console.log(body);
        if(body!=null){
            var $ = cheerio.load(body);
            var title = $("title").text();
              
            var alinks = Object.keys($('a')).map(key=>{
                return $('a')[key];
            });
            for (let target of alinks){
            //$('a').each(async function () {
                
                var link = $(target);
                var text = link.text();
                var href = link.attr("href");
                if(href != null && href.match(urlregex)){
                    if (c>maxPages) return;
                    console.log(currentDepth + ", " + c + ", " + href);
                    c++;
                    var nestedLinks = [];
                    links.push({depth:currentDepth,link:href,tinpmtle:text,links:nestedLinks});
                    await scanUrl(href,nestedLinks,currentDepth+1);
                }
            }
        }
        
    }
}

async function getLinks(){
    try{
        
        await scanUrl(primaryUrl,primaryLinks,0).catch((err) => console.log('caught it'));
            
        console.log("links: "+JSON.stringify(primaryLinks));
        
        fs.writeFileSync('content.json', JSON.stringify(primaryLinks))
    }catch(err){
        console.log("process error",err)
    }
}
