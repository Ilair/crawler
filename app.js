const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require("body-parser");
const api = require('./api.js');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());



app.use('/api',api);

module.exports = app;
