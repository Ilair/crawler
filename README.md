This project is a web crawler, made with Node.js and React.

## Available Scripts

```bash
# Install dependencies (server & client)
npm install
cd client && npm install

# Server only (:9000)
npm start

# Client only (:3000)
npm run client

# Crawler process
node crawlerProcess
```