const express = require('express');
const router = express.Router();
var crawler = require("./crawler");

router.post ('/',async function (req, res, next) {
    try{
        let links = await crawler.getLinks(req.body.url,req.body.maxDepth,req.body.maxPages);
        res.status(200).json({
            links
        });
    }catch(err){

        console.log(err);
        return res.status(500).json({
            error:err
        });
    }

        
    //const links = ["sss","123"];
   
});

module.exports = router;